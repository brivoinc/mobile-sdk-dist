#Brivo Mobile SDK Android
===========================================
A set of reusable libraries, services and components for Java and Kotlin Android apps.
### Installation
```
    defaultConfig {
        minSdkVersion 23
    }
```
Minimum Android version required is 23 in order to use the Brivo Mobile SDK.

Add the following dependencies to your app’s build.gradle configuration
```
dependencies {
    .
    .
    .
    //==================== BRIVO SDK =========================
    implementation(name:'brivoaccess-release', ext:'aar')
    implementation(name:'brivoble-release', ext:'aar')
    implementation(name:'brivoble-core-release', ext:'aar')
    implementation(name:'brivocore-release', ext:'aar')
    implementation(name:'brivoonair-release', ext:'aar')
    implementation(name:'brivolocalauthentication-release', ext:'aar')
    implementation(name:'brivofluid-release', ext:'aar')
    //==================== BRIVO SDK DEPENDENCIES ============
    implementation 'com.google.code.gson:gson:2.9.1'
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.9.1'
    implementation 'org.altbeacon:android-beacon-library:2.19'
    implementation 'androidx.biometric:biometric:1.2.0-alpha04'
    //========================================================
}
```

Add the following permissions to your app’s manifest
```    
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
    
    <service android:name="org.altbeacon.beacon.service.BeaconService"
        android:foregroundServiceType="location"
        tools:node="replace">
        <meta-data android:name="longScanForcingEnabled" android:value="true"/>
    </service>
```

Add the Brivo Fluid service filter in your app's manifest under the application tag
```
<service android:name="org.altbeacon.beacon.service.BeaconService"
    android:foregroundServiceType="location"
    tools:node="replace">
    <meta-data android:name="longScanForcingEnabled" android:value="true"/>
</service>
```
## Usage
To initialize the Brivo Mobile SDK call the initialize method with the application context and with the BrivoConfiguration object
The BrivoConfiguration object requires the clientId and secretId which is provided by Brivo. Valid Brivo URL’s also need to be specified in the Brivo configuration object


```
/**
 * Initializes the BrivoSDK
 *
 * @param context            application context used to initialize SDK data into the application local storage
 * @param brivoConfiguration Brivo client id
 *                           Brivo client secret
 *                           Brivo SDK local storage management enabled
 *                           Brivo SDK should verify door before connecting to it
 */
void init(Context context, BrivoConfiguration brivoConfiguration) throws BrivoSDKInitializationException;
```
#### BrivoSDK init usage 
```
//Java
try {
    BrivoSDK.getInstance().init(getApplicationContext(), new BrivoConfiguration(
        CLIENT_ID,
        CLIENT_SECRET,
        useSDKStorage,
        shouldVerifyDoor));
} catch (BrivoSDKInitializationException | MalformedURLException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDK.getInstance().init(applicationContext, BrivoConfiguration(
            CLIENT_ID,
            CLIENT_SECRET, 
            useSDKStorage,
            shouldVerifyDoor))
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```
The exception is thrown if the SDK is not initialized correctly.
For example one of the parameters is null or missing.

## Brivo Mobile SDK Modules

#### BrivoCore
Initializes the BrivoSDK.
This module also contains the getVersion() method which retuns the SDK version
```
String getVersion()
```
```
//Java
BrivoSDK.getInstance().getVersion()

//Kotlin
BrivoSDK.getInstance().version
```

#### BrivoOnAir
This module manages the connection between the SDK and the Brivo enviroment.
Redeem a Brivo Onair Pass. Brivo Onair Pass allows you to open doors with your smartphone.
```
/**
 * Redeem a Brivo Onair Pass. Brivo Onair Pass allows you to open doors with your smartphone.
 *
 * @param email    Email received from Brivo
 * @param token    Token received from Brivo
 * @param listener listener that handles the success or failure
 */
void redeemPass(String email, String token, IOnRedeemPassListener listener);
```
#### BrivoSDKOnair redeem pass usage 
```
//Java
try {
    BrivoSDKOnair.getInstance().redeemPass(email, token, new IOnRedeemPassListener() {
        @Override
        public void onSuccess(BrivoOnairPass pass) {
            //Manage pass
        }
    
        @Override
        public void onFailed(BrivoError error) {
            //Handle redeem pass error case
        }
    });
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDKOnair.getInstance().redeemPass(email, token, object : IOnRedeemPassListener {
        override fun onSuccess(pass: BrivoOnairPass) {
            //Manage pass
        }

        override fun onFailed(error: BrivoError) {
            //Handle error
        }
    })
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```
Refresh a Brivo Onair Pass.
```
/**
 * Refresh a Brivo Onair Pass. Brivo Onair Pass allows you to open doors with your smartphone.
 *
 * @param brivoTokens accessToken received from Brivo
 *                    refreshToken received from Brivo
 */
void refreshPass(BrivoTokens brivoTokens, IOnRedeemPassListener listener);
```
#### BrivoSDKOnair refresh pass usage 
```
//Java
try {
    BrivoSDKOnair.getInstance().refreshPass(tokens, new IOnRedeemPassListener() {
        @Override
        public void onSuccess(BrivoOnairPass pass) {
            //Manage refreshed pass
        }
    
        @Override
        public void onFailed(BrivoError error) {
            //Handle refresh pass error case
        }
    });
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin

try {
    BrivoSDKOnair.getInstance().refreshPass(tokens, object : IOnRedeemPassListener {
        override fun onSuccess(pass: BrivoOnairPass) {
            //Manage pass
        }

        override fun onFailed(error: BrivoError) {
            //Handle error
        }
    })
} catch (e: BrivoSDKInitializationException) {
    e.printStackTrace()
}
```
Retrieve SDK locally stored passes.
```
/**
 * Retrieved SDK locally stored passes
 *
 * @param listener      listener that handles success or failure
 */
void retrieveSDKLocallyStoredPasses(IOnRetrieveSDKLocallyStoredPassesListener listener);
```
#### BrivoSDKOnair retrieve locally stored passes usage
```
//Java
try {
    BrivoSDKOnair.getInstance().retrieveSDKLocallyStoredPasses(new IOnRetrieveSDKLocallyStoredPassesListener() {
        @Override
        public void onSuccess(LinkedHashMap < String, BrivoOnairPass > passes) {
            //Manage retrieved passes
        }
    
        @Override
        public void onFailed(BrivoError error) {
            //Handle error
        }
    });
} catch (BrivoSDKInitializationException e) {
    e.printStackTrace();
}

//Kotlin
try {
    BrivoSDKOnair.getInstance().retrieveSDKLocallyStoredPasses(object : IOnRetrieveSDKLocallyStoredPassesListener {
        override fun onSuccess(passes: LinkedHashMap<String, BrivoOnairPass>?) {
            //Manage passes
        }

        override fun onFailed(error: BrivoError) {
            //Handle error
        }
    })
} catch (e: BrivoSDKInitializationException) {
    e.printStackTrace()
}
```

#### BrivoAccess
This module handles unlocking of access points internally. It determines what authentication type is required (BLE or NETWORK).
```
/**
 * Sends a request to unlock an access point to BrivoSDK
 *
 * The request will be granted if the card holder has permission to access this door based on their groups affiliation.
 * Available only to digital credential users
 * This method should be used when handing the credentials outside of the SDK
 *
 * @param passId             Brivo passId
 * @param accessPointId      Brivo accessPointId
 * @param cancellationSignal Cancellation signal in order to cancel a BLE communication process
 *                           if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener                 listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
void unlockAccessPoint(String passId, String accessPointId, CancellationSignal cancellationSignal, IOnCommunicateWithAccessPointListenerListener listener);
```
#### BrivoSDKAccess unlock access point usage 
```
//Java
try {
    BrivoSDKAccess.getInstance().unlockAccessPoint(passId, accessPointId, cancellationSignal, new IOnCommunicateWithAccessPointListenerListener() {
        @Override
        public void onSuccess() {
            //Handle unlock access point success case
        }
    
        @Override
        public void onFailed(BrivoError error) {
            //Handle unlock access point error case
        }
    });
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDKAccess.getInstance().unlockAccessPoint(passId, accessPointId, cancellationSignal, object : IOnCommunicateWithAccessPointListenerListener {
        override fun onSuccess() {
            //Handle unlock access point success case
        }

        override fun onFailed(error: BrivoError) {
            //Handle unlock access point error case
        }
    })
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```
This method is called when credentials and data are managed outside of BrivoSDK.
```
/**
 * Sends a request to unlock an access point to BrivoSDK
 * All credentials and data are managed outside of BrivoSDK
 *
 * @param brivoSelectedAccessPoint Brivo accessPointPath (accessPointId, siteId, passId)
 *                                 Brivo bleReaderId
 *                                 Brivo bleCredentials
 *                                 Brivo deviceModelId
 *                                 Brivo doorType
 *                                 Brivo minimumAllowedRssi
 *                                 Unlock attempt time frame
 *                                 BrivoOnairPassCredentials (userId, accessToken, refreshToken)
 * @param cancellationSignal Cancellation signal in order to cancel a BLE communication process
 *                           if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener                 listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
void unlockAccessPoint(BrivoSelectedAccessPoint brivoSelectedAccessPoint, CancellationSignal cancellationSignal, IOnCommunicateWithAccessPointListenerListener listener);
```

#### BrivoSDKAccess unlock access point usage with external credentials
```
//Java
try {
     BrivoSDKAccess.getInstance().unlockAccessPoint(selectedAccessPoint, cancellationSignal, new IOnCommunicateWithAccessPointListener() {
                    @Override
                    public void onResult(@NonNull BrivoResult result) {
                        switch (result.getCommunicationState()) {
                            case SUCCESS:
                                //Handle success
                                break;
                            case FAILED:
                                //Handle failure
                                break;
                            case SHOULD_CONTINUE:
                                //Handle custom action and afterwards perform shouldContinue
                                result.getShouldContinueListener().onShouldContinue(true);
                                break;
                            case SCANNING:
                                //Scanning
                                break;
                            case AUTHENTICATE:
                                //Authenticate
                                break;
                            case CONNECTING:
                                //Connecting
                                break;
                            case COMMUNICATING:
                                //Communication
                                break;
                        }
                    }
                });
            }
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDKAccess.getInstance().unlockAccessPoint(selectedAccessPoint,
            cancellationSignal,
            object : IOnCommunicateWithAccessPointListener {
                override fun onResult(result: BrivoResult) {
                     when (result.communicationState) {
                        AccessPointCommunicationState.SUCCESS -> {
                           //Handle success
                        }
                        AccessPointCommunicationState.FAILED -> {
                           //Handle failure
                        }
                        AccessPointCommunicationState.SHOULD_CONTINUE -> {
                           //Handle custom action and afterwards perform shouldContinue
                            result.shouldContinueListener?.onShouldContinue(true)
                        }
                        AccessPointCommunicationState.SCANNING -> //scanning
                        AccessPointCommunicationState.AUTHENTICATE -> //authenticating
                        AccessPointCommunicationState.CONNECTING -> //connecting
                        AccessPointCommunicationState.COMMUNICATING -> //communicating
                    }
                }
            })
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```

#### BrivoSDKAccess unlock nearest BLE access point using external credentials
```
/**
 * Sends a request to unlock the closest access point to BrivoSDK
 * All credentials and data are managed outside of BrivoSDK
 *
 * @param passes                   Passes needed to unlock nearest bluetooth that is in range
 * @param cancellationSignal       Cancellation signal in order to cancel a BLE communication process
 *                                 if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener                 listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
void unlockNearestBLEAccessPoint(List<BrivoOnairPass> passes, CancellationSignal cancellationSignal, IOnCommunicateWithAccessPointListenerListener listener)
```

#### BrivoSDKAccess unlock nearest BLE access point usage with external credentials

```
//Java
 try {
    BrivoSDKAccess.getInstance().unlockNearestBLEAccessPoint(passes, cancellationSignal, new IOnCommunicateWithAccessPointListener() {
        @Override
        public void onResult(@NonNull BrivoResult result) {
            switch (result.getCommunicationState()) {
                case SUCCESS:
                    //Handle success
                    break;
                case FAILED:
                    //Handle failure
                    break;
                case SHOULD_CONTINUE:
                    //Handle custom action and afterwards perform shouldContinue
                    result.getShouldContinueListener().onShouldContinue(true);
                    break;
                case SCANNING:
                    //Scanning
                    break;
                case AUTHENTICATE:
                    //Authenticate
                    break;
                case CONNECTING:
                    //Connecting
                    break;
                case COMMUNICATING:
                    //Communication
                    break;
            }
        }
    });
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDKAccess.getInstance().unlockNearestBLEAccessPoint(passes, 
        cancellationSignal,
        object : IOnCommunicateWithAccessPointListener {
            override fun onResult(result: BrivoResult) {
                 when (result.communicationState) {
                    AccessPointCommunicationState.SUCCESS -> {
                       //Handle success
                    }
                    AccessPointCommunicationState.FAILED -> {
                       //Handle failure
                    }
                    AccessPointCommunicationState.SHOULD_CONTINUE -> {
                       //Handle custom action and afterwards perform shouldContinue
                        result.shouldContinueListener?.onShouldContinue(true)
                    }
                    AccessPointCommunicationState.SCANNING -> //scanning
                    AccessPointCommunicationState.AUTHENTICATE -> //authenticating
                    AccessPointCommunicationState.CONNECTING -> //connecting
                    AccessPointCommunicationState.COMMUNICATING -> //communicating
            }
        }
    })
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```

#### BrivoSDKAccess unlock nearest BLE access point
```
/**
 * Sends a request to unlock the closest access point to BrivoSDK
 *
 * @param cancellationSignal       Cancellation signal in order to cancel a BLE communication process
 *                                 if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener                 listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
void unlockNearestBLEAccessPoint(CancellationSignal cancellationSignal, IOnCommunicateWithAccessPointListenerListener listener)
```

#### BrivoSDKAccess unlock nearest BLE access point usage
```
//Java
try {
    BrivoSDKAccess.getInstance().unlockNearestBLEAccessPoint(cancellationSignal,new IOnCommunicateWithAccessPointListener() {
        @Override
        public void onResult(@NonNull BrivoResult result) {
            switch (result.getCommunicationState()) {
                case SUCCESS:
                    //Handle success
                    break;
                case FAILED:
                    //Handle failure
                    break;
                case SHOULD_CONTINUE:
                    //Handle custom action and afterwards perform shouldContinue
                    result.getShouldContinueListener().onShouldContinue(true);
                    break;
                case SCANNING:
                    //Scanning
                    break;
                case AUTHENTICATE:
                    //Authenticate
                    break;
                case CONNECTING:
                    //Connecting
                    break;
                case COMMUNICATING:
                    //Communication
                    break;
            }
        }
    });
} catch (BrivoSDKInitializationException e) {
    //Handle BrivoSDK initialization exception
}

//Kotlin
try {
    BrivoSDKAccess.getInstance().unlockNearestBLEAccessPoint(cancellationSignal, 
        cancellationSignal,
        object : IOnCommunicateWithAccessPointListener {
            override fun onResult(result: BrivoResult) {
                 when (result.communicationState) {
                    AccessPointCommunicationState.SUCCESS -> {
                       //Handle success
                    }
                    AccessPointCommunicationState.FAILED -> {
                       //Handle failure
                    }
                    AccessPointCommunicationState.SHOULD_CONTINUE -> {
                       //Handle custom action and afterwards perform shouldContinue
                        result.shouldContinueListener?.onShouldContinue(true)
                    }
                    AccessPointCommunicationState.SCANNING -> //scanning
                    AccessPointCommunicationState.AUTHENTICATE -> //authenticating
                    AccessPointCommunicationState.CONNECTING -> //connecting
                    AccessPointCommunicationState.COMMUNICATING -> //communicating
            }
        }
    })
} catch (e: BrivoSDKInitializationException) {
    //Handle BrivoSDK initialization exception
}
```
#### BrivoBLE
This module manages the connection between an access point and a panel through bluetooth.


#### BrivoConfiguration
This is the module used to configure Allegion Control Locks for the No Tour feature.

### BrivoSDKConfigure configureAccessPoint with internal credentials
```
/**
 * Sends a request to configure an access point to BrivoSDK
 *
 * @param passId             Brivo passId
 * @param accessPointId      Brivo accessPointId
 * @param cancellationSignal Cancellation signal in order to cancel a BLE communication process
 *                           if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener           listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
fun configureAccessPoint(passId: String, accessPointId: String, cancellationSignal: CancellationSignal?, listener: IOnCommunicateWithAccessPointListener)
```

### BrivoSDKConfigure configureAccessPoint with internal credentials usage
```
//Java
BrivoSDKConfigure.getInstance().configureAccessPoint(passId, accessPointId, cancellationSignal, new IOnCommunicateWithAccessPointListener {
       @Override
       public void onResult(@NonNull BrivoResult result) {
            switch (result.getCommunicationState()) {
                case SUCCESS:
                    //Handle success
                    break;
                case FAILED:
                    //Handle failure
                    break;
            }
        }
  });
//Kotlin
BrivoSDKConfigure.getInstance().configureAccessPoint(passId, accessPointId, cancellationSignal, object : IOnCommunicateWithAccessPointListener {
       object : IOnCommunicateWithAccessPointListener {
            override fun onResult(result: BrivoResult) {
                 when (result.communicationState) {
                    AccessPointCommunicationState.SUCCESS -> {
                       //Handle success
                    }
                    AccessPointCommunicationState.FAILED -> {
                       //Handle failure
                    }
            }
        }
  })
```

### BrivoSDKConfigure configureAccessPoint with external credentials
```
/**
 * Sends a request to configure an access point to BrivoSDK
 * All credentials and data are managed outside of BrivoSDK
 *
 * @param brivoSelectedAccessPoint Brivo accessPointPath (accessPointId, siteId, passId)
 *                                 Brivo bleReaderId
 *                                 Brivo bleCredentials
 *                                 Brivo deviceModelId
 *                                 Brivo doorType
 *                                 Brivo minimumAllowedRssi
 *                                 Unlock attempt time frame
 *                                 BrivoOnairPassCredentials (userId, accessToken, refreshToken)
 *
 * @param cancellationSignal Cancellation signal in order to cancel a BLE communication process
 *                           if a null cancellation signal is provided there will be default 30 second timeout
 * @param listener           listener that handles the communication state (SCANNING, AUTHENTICATE, SHOULD_CONTINUE, CONNECTING, COMMUNICATING, SUCCESS, FAILED)
 */
fun configureAccessPoint(brivoSelectedAccessPoint: BrivoSelectedAccessPoint, cancellationSignal: CancellationSignal?, listener: IOnCommunicateWithAccessPointListener)

}
```

### BrivoSDKConfigure configureAccessPoint with external credentials usage
```
//Java
BrivoSDKConfigure.getInstance().configureAccessPoint(selectedAccessPoint, cancellationSignal, new IOnCommunicateWithAccessPointListener {
       @Override
       public void onResult(@NonNull BrivoResult result) {
            switch (result.getCommunicationState()) {
                case SUCCESS:
                    //Handle success
                    break;
                case FAILED:
                    //Handle failure
                    break;
            }
        }
  });
//Kotlin
BrivoSDKConfigure.getInstance().configureAccessPoint(selectedAccessPoint, cancellationSignal, object : IOnCommunicateWithAccessPointListener {
       override fun onResult(result: BrivoResult) {
         when (result.communicationState) {
            AccessPointCommunicationState.SUCCESS -> {
               //Handle success
            }
            AccessPointCommunicationState.FAILED -> {
               //Handle failure
            }
       }
  })
```

#### BrivoLocalAuthentication
This module manages the biometric authentication if an access point requires two factor.
This module also can return the type of biometric authentication (device credentials, fingerprint or face id).

### BrivoSDKLocalAuthentication init 
This method is called in order to initalize the BrivoLocalAuthenticationModule
```
/**
 * Initializes the BrivoLocalAuthentication module
 *
 * @param activity             the activity in which the biometric screen will start from
 * @param title                the title of the biometric prompt
 * @param message              the message of the biometric prompt
 * @param negativeButtonText   the text of the negative button of the biometric prompt
 * @param description          the text of the description of the biometric prompt
 *
 */
void init(Activity activity, String title, String message, String negativeButtonText, String description);
```

BrivoSDKLocalAuthentication init usage
```
//Java
try {
    BrivoSDKLocalAuthentication.getInstance().init(activity,
            title,
            message,
            negativeButtonText,
            description);
} catch (BrivoSDKInitializationException e) {
    e.printStackTrace();
}

//Kotlin
try {
    BrivoSDKLocalAuthentication.getInstance().init(activity,
            title,
            message,
            negativeButtonText,
            description)
} catch (e: BrivoSDKInitializationException) {
    e.printStackTrace()
}
```

### BrivoSDKLocalAuthentication can authenticate method
This method returns if authentication is possible, if it is will return the type of the biometric authentication (device credentials, fingerprint or face id).
```
/**
 * Return if authentication can be performed and, if its the case, the type of the authentication
 * 
 * @param listener             listener that handles if the possibility of authentication is success or failure
 */
void canAuthenticate(@NonNull IOnCanAuthenticateListener listener);
```

BrivoSDKLocalAuthentication can authenticate method usage
```
//Java
try {
    BrivoSDKLocalAuthentication.getInstance().canAuthenticate(new IOnCanAuthenticateListener() {
    @Override
    public void onSuccess(BiometricResult type) {
        //Handle success case
    }

    @Override
    public void onFailed(BrivoError error) {
        //Handleerror case
    }
}););
} catch (BrivoSDKInitializationException e) {
    e.printStackTrace();
}

//Kotlin
try {
    BrivoSDKLocalAuthentication.getInstance().canAuthenticate(object: IOnCanAuthenticateListene {
    @Override
    public void onSuccess(BiometricResult type) {
        //Handle success case
    }

    @Override
    public void onFailed(BrivoError error) {
        //Handle error case
    }
}););
} catch (BrivoSDKInitializationException e) {
    e.printStackTrace();
}
```

### BrivoSDKLocalAuthentication cancel authentication method
```
/**
 * Cancel ongoing authentication
 */
void cancelAuthentication();
```
BrivoSDKLocalAuthentication cancel authentication method usage
```
//Java
BrivoSDKLocalAuthentication.getInstance().cancelAuthentication();

//Kotlin
BrivoSDKLocalAuthentication.getInstance().cancelAuthentication()
```

#### BrivoFluid
This module manages the fluid feature of the Mobile SDK. The module requires background location and bluetooth and can open doors with the screen locked and/or application closed.

### BrivoSDKFluid init 
This method is called in order to initalize the BrivoFluid module
```
/**
 * Initializes the BrivoFluid module
 *
 /**
  * Initializes the BrivoFluid module
  *
  * @param BrivoSDKFluidConfiguration FluidAccessForegroundNotification
  *                                               contains all necessary data for the foreground notification
  *                                               able to set the beacon signal RSSI
  *                                               able to set the bluetooth signal RSSI
  */
@Throws(BrivoSDKInitializationException::class)
fun init(brivoFluidConfiguration: BrivoSDKFluidConfiguration?)
```

BrivoSDKLocalAuthentication init usage
```
//Java
BrivoSDKFluid.INSTANCE.init(new BrivoSDKFluidConfiguration(new FluidAccessForegroundNotification(
        notificationTitle,
        notificationText,
        notificationIconResource,
        notificationId,
        notificationChannelId
        )));

//Kotlin
BrivoSDKFluid.init(BrivoSDKFluidConfiguration(FluidAccessForegroundNotification(
        notificationTitle,
        notificationText,
        notificationIconResource,
        notificationId,
        notificationChannelId
        )))
```

### BrivoSDKFluid start fluid service
This method starts the BrivoFluid service.
```
/**
 * Starts the service for Fluid Access.
 *
 * @param listener       optional listener that is triggered every time an unlock attempt is made and handles success and failure
 */
fun startFluidAccessService(listener: IOnFluidAccessPointUnlockedListener? = null)
```

BrivoSDKFluid start fluid service usage
```
//Java
BrivoSDKFluid.INSTANCE.startFluidAccessService(new IOnFluidAccessPointUnlockedListener() {
    @Override
    public void onSuccess(@NotNull BrivoSelectedAccessPoint selectedAccessPoint) {
        //Handle success case
    }

    @Override
    public void onFailed(@NotNull BrivoError error, @Nullable BrivoSelectedAccessPoint selectedAccessPoint, @Nullable IOnShouldContinueListener listener) {
        //Handle error case
    }
});

//Kotlin
BrivoSDKFluid.startFluidAccessService(object: IOnFluidAccessPointUnlockedListener{
    override fun onSuccess(selectedAccessPoint: BrivoSelectedAccessPoint) {
        //Handle success case
    }

    override fun onFailed(error: BrivoError,selectedAccessPoint: BrivoSelectedAccessPoint?, listener: IOnShouldContinueListener?) {
        //Handle error case
    }
})
```
### BrivoSDKFluid stop fluid service
This method stops the BrivoFluid service.

```
/**
 * Stops the service for Fluid Access.
 */
fun stopFluidAccessService()
```
BrivoSDKFluid stop fluid service usage

```
//Java
BrivoSDKFluid.INSTANCE.stopFluidAccessService();

//Kotlin
BrivoSDKFluid.stopFluidAccessService();
```
### BrivoSDKFluid update fluid access passes
This method updates the current passes externally. This method only works if the Mobile SDK is set with external credentials.
```
/**
 * Updates the OnAirPasses in case the UseSDKStorage is disabled.
 *
 * @param brivoOnairPasses Update the current passes externally
 */
fun updateFluidAccessPasses(brivoOnairPasses: LinkedHashMap<String, BrivoOnairPass>?)
```
BrivoSDKFluid update fluid access passes usage
```
//Java
BrivoSDKFluid.INSTANCE.updateFluidAccessPasses(brivoOnairPasses);

//Kotlin
BrivoSDKFluid.updateFluidAccessPasses(brivoOnairPasses)
```
### BrivoSDKFluid add fluid access pass
This method adds a pass to the current passes externally. This method only works if the Mobile SDK is set with external credentials.
```
/**
 * Updates the OnAirPasses in case the UseSDKStorage is disabled with a single pass. Usually used when redeeming a pass.
 *
 * @param brivoOnairPass  Update with a single pass externally
 */
fun addFluidAccessPass(brivoOnairPass: BrivoOnairPass?)
```
BrivoSDKFluid update fluid access passes usage
```
//Java
BrivoSDKFluid.INSTANCE.addFluidAccessPass(brivoOnairPass);

//Kotlin
BrivoSDKFluid.addFluidAccessPass(brivoOnairPass)
```

Brivo Mobile SDK iOS
===========================================
A set of reusable libraries, services and components for Swift iOS apps.
### Installation
The BrivoSDK components were built using the target version iOS 12.2, Apple Swift version required is 5.1 and Xcode Version 13.4.1 (13F100)

### Permissions
The BrivoSDK requires the following entries in the info.plist:
```
    <key>NSBluetoothAlwaysUsageDescription</key>
    <string>It is used to open Bluetooth access points</string>
    <key>NSBluetoothPeripheralUsageDescription</key>
    <string>It is used to open Bluetooth access points</string>
    <key>NSFaceIDUsageDescription</key>
    <string>Application needs acces to FaceID in order to unlock 2FA doors</string>
```
If Brivo Fluid is used, the following entries need to be added to info.plist:
```
    <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
    <string>Location Services are required for Fluid Access.</string>
    <key>NSLocationWhenInUseUsageDescription</key>
    <string>Location Services are required for Fluid Access.</string>
```
Together with the following background modes for Brivo Fluid:
```
    <key>UIBackgroundModes</key>
    <array>
        <string>bluetooth-central</string>
        <string>location</string>
    </array>
```

The following BrivoSDK components should be added to the application project

```
    BrivoCore.framework
    BrivoAccess.framework
    BrivoNetworkCore.framework
    BrivoOnAir.framework
    BrivoBLE.framework
    BrivoLocalAuthentication.framework
    BrivoFluid.framework
```

In project settings, under "Framework, Libraries, and Embedded content" section, the frameworks must be marked as "Embed & Sign".

## Usage
Before using the Brivo Mobile SDK it is mandatory to configure (through instance) of BrivoSDK class with a BrivoConfiguration object
The BrivoConfiguration object requires a set of parameters listed bellow:
```
/**
 * Configure the BrivoSDK parameter
 *
 * Parameters:
 * brivoConfiguration  Brivo client id
 *                     Brivo client secret
 *                     Brivo SDK local storage management enabled
 */
```
#### BrivoSDK configuration usage 
When shouldPromptForContinuation is set to true, Brivo Mobile SDK will call the completion block onResult with shouldContinue state. 
AccessPointPath will be populated with information about the door that will be open. The unlocking process can be continued or stopped by calling shouldContinue method on result object received in callback.
Brivo Mobile SDK will freeze the unlocking process until the method shouldContinue is called.

IMPORTANT: shouldContinue method should be called every time, even if the unlocking process should be stopped.
shouldContinue(true) -> Unlocking process should continue
shouldContinue(false) -> Unlocking process should stop

```

do {
    let brivoConfiguration = try BrivoConfiguration(clientId: "CLIENT_ID",
                                                    clientSecret: "CLIENT_SECRET",
                                                    useSDKStorage: USE_SDK_STORAGE,
                                                    shouldPromptForContinuation: SHOULD_PROMPT_FOR_CONTINUATION)
    BrivoSDK.instance.configure(brivoConfiguration: brivoConfiguration)
} catch let error {
    //Handle BrivoSDK configuration exception
}
```

The exception is thrown if the BrivoConfiguration class is not initialized correctly.
For example one of the parameters is nil or empty string.

## Brivo Mobile SDK Modules

#### BrivoCore
This module implements the Brivo SDK class that is accessible through 'instance' property. It has the following responsibilities:
```
func getBrivoConfiguration() throws -> BrivoConfiguration
var version: String
```

#### BrivoOnAir
This module manages the connection between the application and the Brivo environment. It has the following responsibilities:
```
/**
 * Redeeming a Brivo Onair Pass. Brivo Onair Pass gives you the access to a site and allows you to open access points.
 *
 * Parameters:
 * passId     Pass ID (email) received from Brivo
 * passCode   Pass Code received from Brivo
 * onSuccess  Completion block that handles the success of operation
 * onFailure  Completion block that handles the failure of operation
 */
func redeemPass(passId: String, 
                passCode: String, 
                onSuccess: RedeemPassOnSuccessType?, 
                onFailure: OnFailureType?)
                               
/**
 * Refreshing a Brivo Onair Pass.
 *
 * Parameters:
 * brivoTokens accessToken received from Brivo
 *             refreshToken received from Brivo
 * onSuccess   Completion block that handles the success of operation
 * onFailure   Completion block that handles the failure of operation
 */
func refreshPass(brivoTokens: BrivoTokens,
                 onSuccess: RefreshPassOnSuccessType?,
                 onFailure: OnFailureType?)

/**
 * Retrieving the BrivoSDK locally stored passes.
 *
 * Parameters:
 * onSuccess   Completion block that handles the success of operation
 * onFailure   Completion block that handles the failure of operation
 */
func retrieveSDKLocallyStoredPasses(onSuccess: RetrieveSDKLocallyStoredPassesOnSuccess?,
                                    onFailure: OnFailureType?) throws
```

#### BrivoSDKOnair redeem pass usage 
```
do {
    try BrivoSDKOnAir.instance().redeemPass(passId: "PASS_ID",
                                            passCode: "PASS_CODE",
                                            onSuccess: { [weak self] (brivoOnAirPass) in
                                                //Manage pass
                                            }) {[weak self] (responseStatus) in
                                                //Handle redeem pass error case
                                            }
} catch let error {
    //Handle BrivoSDK initialization exception
}
```
#### BrivoSDKOnair refresh pass usage 
```
do {
   try BrivoSDKOnAir.instance().refreshPass(brivoTokens: tokens, 
                                            onSuccess: {[weak self] (refreshedPass) in
                                                //Manage refreshed pass
                                            }) {[weak self] (responseStatus) in
                                                //Handle refresh pass error case
                                            }
} catch let error {
   //Handle BrivoSDK initialization exception
}
```

#### BrivoSDKOnair retrieve locally stored passes usage
```
do {
    try BrivoSDKOnAir.instance().retrieveSDKLocallyStoredPasses(onSuccess: { [weak self] (brivoOnAirPasses) in
                                                                    //Manage retrieved passes
                                                                }) { [weak self] (status) in
                                                                    //Handle passes retrieval error case
                                                                }
} catch let error {
    //Handle BrivoSDK initialization exception
}
```

#### BrivoAccess
This module provides a simplified interface of unlocking access points either Bluetooth type or Internet type. It has the following responsibilities:
```
/**
 * Sends a request to BrivoSDK to unlock an access point.
 * This method is called when credentials and data are managed inside of BrivoSDK.
 *
 * Parameters:
 * passId                   Brivo pass id
 * accessPointId            Brivo access point id
 * onResult:                Completion block that represent different access point communication states (scanning, shouldContinue, connecting, communicating, success, failed)
 * cancellationSignal       Cancellation signal for a customized BLE unlock timeout
 *                              if a null cancellation signal is provided the default timeout of 30 seconds will be used
 */
 
 func unlockAccessPoint(passId: String,
                        accessPointId: String,
                        onResult: OnResultType?,
                        cancellationSignal: CancellationSignal?)
                       
/**
 * Sends a request to BrivoSDK to unlock an access point.
 * This method is called when credentials and data are managed outside of BrivoSDK.
 *
 * Parameters:
 * selectedAccessPoint      Brivo Selected Access Point to unlock
 * onResult:                Completion block that represent different access point communication states (scanning, shouldContinue, connecting, communicating, success, failed)
 * cancellationSignal       Cancellation signal for a customized BLE unlock timeout
 *                              if a null cancellation signal is provided the default timeout of 30 seconds will be used
 */
 
 func unlockAccessPoint(selectedAccessPoint: BrivoSelectedAccessPoint,
                        onResult: OnResultType?,
                        cancellationSignal: CancellationSignal?)
```

#### BrivoSDKAccess unlock access point usage with internal stored credentials
```
do {
    try BrivoSDKAccess.instance().unlockAccessPoint(passId: "PASS_ID",
                                                    accessPointId: "ACCESS_POINT_ID",
                                                    onResult: { [weak self] result in
                                                       switch result.accessPointCommunicationState {
                                                       case .success:
                                                           // handle success state
                                                       case .failed:
                                                           // handle failed state
                                                       default: break
                                                       }
} catch let error {
    //Handle BrivoSDK initialization exception
}
```

#### BrivoSDKAccess unlock access point usage with external credentials 
```
@objc public required init(accessPointPath: AccessPointPath,
                           doorType: DoorType,
                           passCredential: BrivoOnairPassCredentials,
                           isTwoFactorEnabled: Bool = false,
                           readerUid: String? = nil,
                           bleCredentials: String? = nil,
                           timeframe: Int32 = 0,
                           deviceModelId: String?)
do {
    let selectedAccessPoint = BrivoSelectedAccessPoint(accessPointPath: ...,
                                                        doorType: ...,
                                                        passCredential: ...,
                                                        isTwoFactorEnabled: ...,
                                                        readerUid: ...,
                                                        bleCredentials: ...,
                                                        timeframe: ...,
                                                        deviceModelId: ...) 
                                                        
                                                        @objc public func unlockAccessPoint(selectedAccessPoint: BrivoSelectedAccessPoint,
                                                                                            onResult: OnResultType?,
                                                                                            cancellationSignal: CancellationSignal?)
    
    try BrivoSDKAccess.instance().unlockAccessPoint(selectedAccessPoint: selectedAccessPoint,
                                                    onResult: { [weak self] (result) in
                                                        if result.accessPointCommunicationState == .success {
                                                            //Handle unlock access point success case
                                                        } else if result.accessPointCommunicationState == .failed {
                                                            //Handle unlock access point failed case
                                                        } else {
                                                            //Handle unlock access point other cases
                                                        }
                                                    }, cancellationSignal: nil) 
} catch let error {
    //Handle BrivoSDK initialization exception
}
```

#### BrivoBLE
This module manages the connection between an access point and a panel through bluetooth.

#### BrivoFluid
### BrivoSDKFluid start fluid service
This method starts the BrivoFluid service.
```
/**
 Starts the fluid access service
 The service listens for iBeacon compatible access points.
 The access points wouldn't automatically advertisement the iBeacon signal, usually a user interaction is needed to trigger the signal.
 When the service intercepts a signal it will try to automatically open the access point over bluetooth.

 The service can work in background if the application is not terminated.
 For the service to work in the background, the application needs always access to the location.
 And the following background modes:  location, bluetooth-central, audio and processing.

 The request will be granted if the card holder has permission to access this door based on their groups affiliation.
 Available only to digital credential users

 - Parameter onSuccess: completion block that handles success for every access point that is unlocked
 - Parameter onFailure: completion block that handles failure for every access point that failed to unlock
 - Parameter minimumPanelRssi: The minimum RSSI value that should be used to unlock a specific panel.
 - Parameter minimumBeaconRssi: The minimum RSSI value that should be used to detect an iBeacon.
 */

func startFluidAccessService(onSuccess: OnFluidUnlockedAccessPointSuccessType?, onFailure: OnFailureType?, minimumPanelRssi: Int, minimumBeaconRssi: Int)
```

BrivoSDKFluid start fluid service usage
```
func setupFluidAccess() {
    if userDefaultsService.settingEnabled(forKey: .fluidAccessEnabled) {
        BrivoSDKFluid.instance().startFluidAccessService(onSuccess: ({
            handle OnSuccess
        }), onFailure: ({ 
            handle OnFailure
        }), minimumPanelRssi: Constants.fluidPanelMinimumRssi, minimumBeaconRssi: Constants.fluidBeaconMinimumRssi)
    }
}

    BrivoSDKFluid.instance().startFluidAccessService(minimumPanelRssi: Constants.fluidPanelMinimumRssi,
                                                     minimumBeaconRssi: Constants.fluidBeaconMinimumRssi,
                                                     onResult: { [weak self] result in
                                                        switch result.accessPointCommunicationState {
                                                        case .success:
                                                            // Handle success case
                                                        case .failed:
                                                            // Handle failed case
                                                        default:
                                                            break
                                                        }
                                                     })
```
### BrivoSDKFluid stop fluid service
This method stops the BrivoFluid service.

```
/**
 Stops the fluid access service
 */
func stopFluidAccessService()
```
BrivoSDKFluid stop fluid service usage

```
BrivoSDKFluid.instance().stopFluidAccessService()
```
### BrivoSDKFluid update fluid access passes
This method updates the current passes externally. This method only works if the Mobile SDK is set with external credentials.
```
/**
 Update the OnAirPasses in case the credentials are not managed by the SDK.
 If the SDK does not manage OnAirPasses internally, this method should be called when updates to the passes were made.
 - Parameter brivoOnairPasses: The complete array of BrivoOnairPasses that the fluid access service should use.
 */
func updateFluidAccessPasses(brivoOnairPasses: [BrivoOnairPass]?)
```
BrivoSDKFluid update fluid access passes usage
```
BrivoSDKFluid.instance().updateFluidAccessPasses(brivoOnairPasses: filteredPasses)
```
### BrivoSDKFluid add fluid access pass
This method adds a pass to the current passes externally. This method only works if the Mobile SDK is set with external credentials.
```
/**
 Add a new BrivoOnairPass to the fluid access service.
 If the SDK does not manage OnAirPasses internally, this method should be called when a new BrivoOnairPass is added.
 - Parameter brivoOnairPass: The BrivoOnairPass that should be added to the fluid access service
 */
func addFluidAccessPass(brivoOnairPass: BrivoOnairPass?)
```
BrivoSDKFluid update fluid access passes usage
```
BrivoSDKFluid.instance().addFluidAccessPass(brivoOnairPass: pass)
```


### BrivoSDKFluid delegate
This delegate can be implemented and offers additional functionality when using Fluid.
protocol IBrivoSDKFluidDelegate
```
/**
 If the delegate is implemented, the Fluid will call this callback every time it wants to unlock an access point.
 If the delegate is not implemented, every access point discovered by Fluid will be unlocked.

 - Parameter brivoSDKFluid: The instance of Fluid that wants to open an access point
 - Parameter accessPoint: The access point that will be unlocked
 - Parameter completion: If the completion is called with the value true, the Fluid will begin the process of unlocking the access point.
                         If the completion is called with the value false, the Fluid will stop the proccess.
                         It is required to call this callback once the delegate is implemented.
 **/
func brivoSDKFluidShouldUnlockAccessPoint(_ brivoSDKFluid: BrivoSDKFluid,
                                          accessPoint: BrivoSelectedAccessPoint,
                                          completion: @escaping (Bool) -> Void)
```
